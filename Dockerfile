FROM rikorose/gcc-cmake:latest


# Install dependecies
RUN apt-get update && apt-get install -y \
	flex \
	bison \
     && rm -rf /var/lib/apt/lists/*

# Install doxygen
RUN git clone https://github.com/doxygen/doxygen.git && \
	cd doxygen && \
	mkdir build && \
	cd build && \
	cmake -G "Unix Makefiles" .. && \
	make && \
	make install && \
	make clean

# The default command
CMD ["/bin/sh"]


