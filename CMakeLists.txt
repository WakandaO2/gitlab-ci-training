cmake_minimum_required(VERSION 3.7)
project(gitlab-ci-training)

set(CMAKE_C_STANDARD 99)

INCLUDE_DIRECTORIES(contrib/queue)

set(SOURCE_FILES graph.c graph.h errors.h graph_utils.c graph_utils.h)
add_library(graph STATIC ${SOURCE_FILES})

ENABLE_TESTING()
ADD_SUBDIRECTORY( tests )
add_subdirectory(docs)